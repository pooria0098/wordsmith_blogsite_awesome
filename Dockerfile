FROM python:3.8.5
LABEL MAINTAINER="Pooria Tavana"

WORKDIR /blogpy

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ADD requirements.txt /blogpy
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /blogpy

RUN python manage.py collectstatic --no-input

#CMD ["gunicorn", "WordSmith.wsgi.application", "--bind", "0.0.0.0:8000"]