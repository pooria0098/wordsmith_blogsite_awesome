from datetime import datetime

from django.contrib.auth.models import User
from django.utils.html import strip_tags
from rest_framework import serializers
from blog.models import Article, UserProfile, Category


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.user.username')

    class Meta:
        model = Article
        fields = [
            'url',
            'id',
            'title',
            'display_category',
            'cover',
            'content',
            'promote',
            'language',
            'status',
            'publishedTime',
            'author'
        ]
        extra_kwargs = {
            'publishedTime': {'read_only': True},
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['content'] = strip_tags(instance.content)
        return data


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_active',
            'is_superuser'
        ]


class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = [
            'url',
            'user',
            'avatar',
            'description',
            'article_set'
        ]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['description'] = strip_tags(instance.description)
        return data


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = [
            'url',
            'title',
            'cover',
            'article_set'
        ]

# class ArticleSerializer_2(serializers.Serializer):
#     title = serializers.CharField(max_length=100)
#     slug = serializers.CharField(max_length=100)
#     cover = serializers.FileField(default='default_article_cover.jpeg', allow_empty_file=True, allow_null=True)
#     content = serializers.CharField()
#     # category_id = serializers.IntegerField(required=True, allow_null=False)
#     author_id = serializers.IntegerField(required=True, allow_null=False)
#     promote = serializers.BooleanField(required=True, allow_null=False)
#     publishedTime = serializers.DateTimeField(default=datetime.now, allow_null=True)


# class ArticleSerializer_3(serializers.ModelSerializer):
#     author = serializers.ReadOnlyField(source='author.user.username')
#
#     class Meta:
#         model = Article
#         fields = [
#             'id',
#             'title',
#             'display_category',
#             'cover',
#             'content',
#             'promote',
#             'language',
#             'status',
#             'publishedTime',
#             'author'
#         ]
#         extra_kwargs = {
#             'publishedTime': {'read_only': True},
#         }


# class SubmitArticleSerializer(serializers.Serializer):
#     title = serializers.CharField(required=True, allow_null=False, allow_blank=False, max_length=128)
#     cover = serializers.FileField(required=True, allow_null=False, allow_empty_file=False)
#     content = serializers.CharField(required=True, allow_null=False, allow_blank=False, max_length=2048)
#     category_id = serializers.IntegerField(required=True, allow_null=False)
#     author_id = serializers.IntegerField(required=True, allow_null=False)
#     promote = serializers.BooleanField(required=True, allow_null=False)
#     publishedTime = serializers.DateTimeField(default=datetime.now, allow_null=True)

# class UpdateCoverArticleSerializer(serializers.Serializer):
#     cover = serializers.FileField(required=True, allow_empty_file=False)
