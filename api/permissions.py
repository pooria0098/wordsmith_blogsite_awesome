from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # print(str(obj.author))
        # print(type(str(obj.author)))
        # print(request.user.first_name + ' ' + request.user.last_name)
        # print(type(request.user.first_name + ' ' + request.user.last_name))

        if request.method in permissions.SAFE_METHODS:
            return True
        return str(obj.author) == request.user.first_name + ' ' + request.user.last_name
