from django.urls import path, include

from . import views

# urlpatterns = [
# path('', views.API_Root.as_view()),

# path('article/all/', views.AllArticleAPIView.as_view(), name='all_article'),
# path('article/submit/', views.SubmitArticleAPIView.as_view(), name='submit_article'),
# path('article/<int:pk>/update_cover/', views.UpdateCoverArticleAPIView.as_view(), name='update_cover_article'),
# path('article/<int:pk>/update/', views.UpdateArticleAPIView.as_view(), name='update_article'),
# path('article/<int:pk>/delete/', views.DeleteArticleAPIView.as_view(), name='delete_article'),

# path('search/article/title/', views.TitleArticleAPIView.as_view(), name='title_article'),
# http://localhost:8000/api/v1/search/article/title/?article_title=java
# path('search/article/content/', views.ContentArticleAPIView.as_view(), name='content_article'),
# http://localhost:8000/api/v1/search/article/content/?article_content=جاوا
# path('search/article/author/', views.AuthorArticleAPIView.as_view(), name='author_article'),
# http://localhost:8000/api/v1/search/article/author/?author=pooria

# path('articles/', views.ArticleList.as_view(), name='article-list'),
# path('articles/<int:pk>/', views.ArticleDetail.as_view(), name='article-detail'),

# path('users/', views.UserList.as_view(), name='userprofile-list'),
# path('users/<int:pk>/', views.UserDetail.as_view(), name='userprofile-detail'),
#
# path('categories/', views.CategoryList.as_view(), name='category-list'),
# path('category/<int:pk>/', views.CategoryDetail.as_view(), name='category-detail'),
# ]

# ----------------- Dynamic Binding --------------------
from rest_framework.routers import DefaultRouter
from .views import UserViewSet, CategoryViewSet, ArticleViewSet
from .views import TitleArticleAPIView, ContentArticleAPIView, AuthorArticleAPIView

router = DefaultRouter()
router.register('users', UserViewSet)
router.register('articles', ArticleViewSet)
router.register('categories', CategoryViewSet)
urlpatterns = [
    path('search/article/title/', TitleArticleAPIView.as_view(), name='title_article'),
    # http://localhost:8000/api/v1/search/article/title/?article_title=java
    path('search/article/content/', ContentArticleAPIView.as_view(), name='content_article'),
    # http://localhost:8000/api/v1/search/article/content/?article_content=جاوا
    path('search/article/author/', AuthorArticleAPIView.as_view(), name='author_article'),
    # http://localhost:8000/api/v1/search/article/author/?author=pooria
]
urlpatterns += router.urls
