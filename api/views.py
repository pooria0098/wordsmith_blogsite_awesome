from django.contrib.auth.models import User
from django.contrib.postgres.search import TrigramSimilarity
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.db.models.functions import Greatest

from rest_framework import status, permissions, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from .permissions import IsOwnerOrReadOnly

from blog.models import Category, UserProfile, Article

# -------------- class-based views --------------------

# class AllArticleAPIView(APIView):
#     permission_classes = [permissions.IsAuthenticated]
#
#     def get(self, request):
#         try:
#             all_articles = Article.objects.all().order_by('-createdTime')
#
#             # Scenario_1
#             # data = []
#             # for article in all_articles:
#             #     data.append({
#             #         'title': article.title,
#             #         'slug': article.slug,
#             #         'cover': 'http://127.0.0.1:8000'+article.cover.url if article.cover else 'Not Found',
#             #         'content': article.content,
#             #         'publishedTime': article.publishedTime.date(),
#             #         'category': article.display_category(),
#             #         'author': article.author.user.first_name + ' ' + article.author.user.last_name,
#             #         'promote': article.promote,
#             #     })
#             # return Response({'data': data}, status=status.HTTP_200_OK)
#
#             # Scenario_2
#             # serialized_data = ArticleSerializer_2(all_articles, many=True, context={'request': request})
#             # return Response({'data':serialized_data.data}, status=status.HTTP_200_OK)
#
#             # Scenario_3
#             serialized_data = ArticleSerializer_3(all_articles, many=True, context={'request': request})
#             return Response({'data': serialized_data.data}, status=status.HTTP_200_OK)
#
#         except:
#             return Response({'status': "Internal Server Error , We'll Check It Later"},
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# class SubmitArticleAPIView(APIView):
#     permission_classes = [permissions.IsAdminUser]
#
#     def post(self, request):
#         print(request.data)
#         try:
#             serializer = SubmitArticleSerializer(data=request.data)
#             if serializer.is_valid():
#                 title = serializer.data.get('title')
#                 cover = request.FILES['cover']
#                 content = serializer.data.get('content')
#                 category_id = serializer.data.get('category_id')
#                 author_id = serializer.data.get('author_id')
#                 promote = serializer.data.get('promote')
#                 publishedTime = serializer.data.get('publishedTime')
#             else:
#                 print(serializer.errors)
#                 return Response({'status': 'Bad Request.'},  # this status shows in body http response
#                                 status=status.HTTP_400_BAD_REQUEST)  # this status shows in header http response
#
#             user = User.objects.get(id=author_id)
#             author = UserProfile.objects.get(user=user)
#             category = Category.objects.get(id=category_id)
#
#             article = Article()
#             article.title = title
#             article.cover = cover
#             article.content = content
#             article.category = category
#             article.author = author
#             article.promote = promote
#             article.publishedTime = publishedTime
#             article.save()
#
#             return Response({'status': 'OK'}, status=status.HTTP_200_OK)
#
#         except:
#             return Response({'status': "Internal Server Error, We'll Check It Later"},
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# class UpdateCoverArticleAPIView(APIView):
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]
#
#     def get(self, request, pk):
#         query = Article.objects.get(pk=pk)
#         serializers = ArticleSerializer_3(query, many=False, context={'request': request})
#         return Response(serializers.data, status=status.HTTP_200_OK)
#
#     def post(self, request, pk):
#         print(request.data)
#         print(self.request.data)
#         try:
#             query = Article.objects.get(pk=pk)
#             serializer = UpdateCoverArticleSerializer(query, data=request.data)
#             if serializer.is_valid():
#                 cover = request.FILES['cover']
#             else:
#                 print(serializer.errors)
#                 return Response({'status': 'Bad Request.'}, status=status.HTTP_400_BAD_REQUEST)
#
#             Article.objects.filter(pk=pk).update(cover=cover)
#             return Response({'status': 'OK'}, status=status.HTTP_200_OK)
#
#         except:
#             return Response({'status': "Internal Server Error, We'll Check It Later"},
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# class UpdateArticleAPIView(APIView):
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]
#
#     def get(self, request, pk):
#         print(self.request)
#         query = Article.objects.get(pk=pk)
#         serializers = ArticleSerializer(query, many=False, context={'request': request})
#         return Response(serializers.data, status=status.HTTP_200_OK)
#
#     def put(self, request, pk):
#         print(self.request)
#         print('-----------------------------')
#         print(self.request.data)
#         print('-----------------------------')
#         query = Article.objects.get(pk=pk)
#         serializer = ArticleSerializer(query, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class DeleteArticleAPIView(APIView):
#     permission_classes = [permissions.IsAdminUser]
#
#     def get(self, request, pk):
#         print(self.request)
#         query = Article.objects.get(pk=pk)
#         serializers = ArticleSerializer(query, many=False, context={'request': request})
#         return Response(serializers.data, status=status.HTTP_200_OK)
#
#     def delete(self, request, pk):
#         try:
#             query = Article.objects.get(pk=pk)
#             query.delete()
#             return Response({'status': 'Deleted'}, status=status.HTTP_204_NO_CONTENT)
#         except:
#             return Response({'status': "Internal Server Error, We'll Check It Later"},
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# -------------- generic class-based views --------------------
# class ArticleList(generics.ListCreateAPIView):
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]
#     queryset = Article.objects.all().order_by('-createdTime')
#     serializer_class = ArticleSerializer
#
#     def perform_create(self, serializer):
#         serializer.save(author=self.request.user)
#
#
# class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
#     permission_classes = [IsOwnerOrReadOnly]
#     queryset = Article.objects.all()
#     serializer_class = ArticleSerializer

# class UserList(generics.ListAPIView):
#     queryset = UserProfile.objects.all()
#     serializer_class = UserProfileSerializer
#
#
# class UserDetail(generics.RetrieveAPIView):
#     queryset = UserProfile.objects.all()
#     serializer_class = UserProfileSerializer

# class CategoryList(generics.ListAPIView):
#     queryset = Category.objects.all()
#     serializer_class = CategorySerializer
#
#
# class CategoryDetail(generics.RetrieveAPIView):
#     queryset = Category.objects.all()
#     serializer_class = CategorySerializer

# class API_Root(APIView):
#     def get(self, request, format=None):
#         return Response({
#             'users': reverse('userprofile-list', request=request, format=format),
#             'articles': reverse('article-list', request=request, format=format),
#             'categories': reverse('category-list', request=request, format=format),
#         })

# -------------- ViewSets --------------------
from .serializers import UserProfileSerializer, CategorySerializer, ArticleSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all().order_by('-createdTime')
    serializer_class = ArticleSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class TitleArticleAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        try:
            article_title = request.GET['article_title']

            try:
                specific_article = Article.objects.filter(title__icontains=article_title)
            except ObjectDoesNotExist:
                return Response({'Not Found'}, status=status.HTTP_404_NOT_FOUND)

            serialized_data = ArticleSerializer(specific_article, many=True, context={'request': request})
            return Response({'data': serialized_data.data}, status=status.HTTP_200_OK)

        except:
            return Response({'status': "Internal Server Error , We'll Check It Later"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ContentArticleAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        try:
            article_content = request.GET['article_content']
            # specific_article = Article.objects.annotate(
            #     similarity=Greatest(
            #         TrigramSimilarity('content', article_content),
            #         TrigramSimilarity('title', article_content),
            #     )).filter(similarity__gt=0.1).order_by('-similarity')
            specific_article = Article.objects.filter(content__icontains=article_content)
            serialized_data = ArticleSerializer(specific_article, many=True, context={'request': request})
            return Response({'data': serialized_data.data}, status=status.HTTP_200_OK)

        except:
            return Response({'status': "Internal Server Error , We'll Check It Later"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class AuthorArticleAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        try:
            author = request.GET['author']
            specific_article = Article.objects.filter(Q(author__user__first_name__icontains=author) |
                                                      Q(author__user__last_name__icontains=author) |
                                                      Q(author__user__username__icontains=author))
            serialized_data = ArticleSerializer(specific_article, many=True, context={'request': request})

            return Response({'data': serialized_data.data}, status=status.HTTP_200_OK)

        except:
            return Response({'status': "Internal Server Error , We'll Check It Later"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
