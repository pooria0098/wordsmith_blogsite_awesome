from django.db.models import Q
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from .models import Article, ContactUs, HandleBaseEmail
from .forms import ContactUsForm, SubscribeForm, CreateArticleForm


class ArticlesListView(ListView):
    model = Article
    template_name = 'blog/index.html'
    paginate_by = 4
    queryset = Article.objects.filter(status__exact='published').order_by('publishedTime')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ArticlesListView, self).get_context_data(**kwargs)
        all_promote_articles = Article.objects.filter(promote=True)
        context.update({
            'all_promote_articles': all_promote_articles,
        })
        return context


class CategoryArticlesListView(ListView):
    model = Article
    paginate_by = 4
    template_name = 'blog/category.html'
    queryset = Article.objects.filter(status__exact='published').order_by('-publishedTime')


class CategoryWebArticlesListView(ListView):
    model = Article
    paginate_by = 4
    template_name = 'blog/category_web.html'
    queryset = Article.objects.filter(Q(category__title__contains='Web') & Q(status__exact='published')).order_by(
        '-publishedTime')


class CategoryAndroidArticlesListView(ListView):
    model = Article
    paginate_by = 4
    template_name = 'blog/category_android.html'
    queryset = Article.objects.filter(category__title__contains='Android', status__exact='published').order_by(
        '-publishedTime')


class CategoryDesktopArticlesListView(ListView):
    model = Article
    paginate_by = 4
    template_name = 'blog/category_desktop.html'
    queryset = Article.objects.filter(category__title__contains='Desktop', status__exact='published').order_by(
        '-publishedTime')


class ContactPage(CreateView):
    form_class = ContactUsForm
    model = ContactUs
    template_name = 'blog/page-contact.html'


class Subscribe(CreateView):
    form_class = SubscribeForm
    model = HandleBaseEmail
    template_name = 'blog/base_with_feature.html'


class AboutPage(TemplateView):
    template_name = 'blog/page-about.html'


class SingleStandardPage(TemplateView):
    template_name = 'blog/single-standard.html'


class VideoPage(TemplateView):
    template_name = 'blog/single-video.html'


class AudioPage(TemplateView):
    template_name = 'blog/single-audio.html'


class ArticleDetailView(DetailView):
    model = Article


# CRUD
class CreateArticle(LoginRequiredMixin, generic.CreateView):
    form_class = CreateArticleForm
    template_name = 'api/article_create.html'
    success_url = '/'


class UpdateArticle(LoginRequiredMixin, generic.UpdateView):
    model = Article
    form_class = CreateArticleForm
    template_name = 'api/article_create.html'
    success_url = '/'


class DeleteArticle(LoginRequiredMixin, generic.DeleteView):
    model = Article
    template_name = 'api/confirm_delete.html'
    success_url = '/'
