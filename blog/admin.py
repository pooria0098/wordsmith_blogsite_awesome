from django.contrib import admin
from django.contrib.auth.models import Permission
from django.http import HttpResponse
from django.core import serializers
from django.utils.html import format_html
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from .models import Article, Category, UserProfile, ContactUs, HandleBaseEmail
from admin_export_action.admin import export_selected_objects
from django.contrib.auth.models import User


# Actions
def make_published(modelAdmin, request, querySet):
    result = querySet.update(status='published')

    if result == 1:
        message = "یک پست"
    else:
        message = "{} پست".format(result)

    modelAdmin.message_user(request, "{} با موفقیت منتشر شد.".format(message))


def make_draft(modelAdmin, request, querySet):
    result = querySet.update(status='draft')

    if result == 1:
        message = "یک پست"
    else:
        message = "{} پست".format(result)

    modelAdmin.message_user(request, "{} باموفقیت آماده انتشار شد.".format(message))


def make_withdrawn(modelAdmin, request, queryset):
    result = queryset.update(status='withdrawn')

    if result == 1:
        message = "یک پست"
    else:
        message = "{} پست".format(result)

    modelAdmin.message_user(request, "{} با موفقیت برداشته شد.".format(message))


def export_as_json(modelAdmin, request, queryset):
    response = HttpResponse(content_type='Application/json')
    serializers.serialize("json", queryset, stream=response)
    return response


make_published.short_description = 'انتشار پست های انتخاب شده'
make_draft.short_description = 'آماده انتشار پست های انتخاب شده'
make_withdrawn.short_description = 'برداشتن پست های انتخاب شده'
export_as_json.short_description = 'استخراج Json پست های انتخاب شده'
export_selected_objects.short_description = 'استخراج با فرمت دلخواه پست های انتخاب شده'


# admin.site.register('auth.User')

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    fields = [('user', 'color_code'), 'avatar', 'description']
    list_display = ('user', 'avatar', 'coloredDescription')
    search_fields = ('user',)
    list_display_links = ('user', 'avatar',)
    ordering = ['user']
    actions = [export_selected_objects, ]

    def coloredDescription(self, obj):
        return format_html(
            '<span style="color: #{};">{}</span>'.format(obj.color_code, obj.description)
        )

    coloredDescription.short_description = 'توضیحات'


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'display_category', 'author', 'publishedTime', 'updatedTime', 'createdTime', 'status', 'promote')
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('publishedTime', 'status', 'author', 'category', 'promote')
    search_fields = ('title', 'category', 'author')
    ordering = ['status', 'publishedTime']
    actions = [make_published, make_draft, make_withdrawn, export_as_json, export_selected_objects, ]
    list_editable = ('status', 'promote', 'author',)

    fieldsets = (
        (None, {'fields': ('title', 'slug', 'content')}),
        ('extraFeatures',
         {'fields': ('category', 'author', 'language', 'cover', 'status', 'promote'), 'classes': ['collapse']}),
        ('DateTime', {'fields': ('publishedTime',)}),
    )


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'cover', 'created_at')
    list_display_links = ('cover', 'title',)
    ordering = ['created_at']
    actions = [export_selected_objects, ]


admin.site.register(Category, CategoryAdmin)


@admin.register(ContactUs)
class ContactUsAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')
    list_display_links = ('email',)
    ordering = ['-date']
    actions = [export_selected_objects, ]


admin.site.register(HandleBaseEmail)

admin.site.register(Permission)
