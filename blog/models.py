from datetime import datetime
from PIL import Image
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.urls import reverse


# One-to-one
# user=User()
# user.userprofile
# profile=UserProfile()
# profile.user
# ---
# from blog.models import UserProfile
# from django.contrib.auth.models import User
# profile=UserProfile.objects.get(user__username='Pooria')
# profile.user
# user=User.objects.get(username='Pooria')
# user.userprofiel

# ForeignKey
# new_article=Article()
# new_article.author
# new_author=UserProfile()
# new_author.article_set
# ---
# from blog.models import UserProfile,Article
# new_article=Article.objects.get(author__user__username='Ali')
# new_article.author
# pooria=UserProfile.objects.get(user__username='Pooria')
# pooria.article_set.all()

# Many-to-many
# new_article=Article()
# new_article.??? --> display_category
# new_category=Category()
# new_category.article_set
# ---
# from blog.models import Category,Article
# category.article_set.all()
# category=Category.objects.get(title='Web')
# >>> article=Article.objects.get(title='Java Article')
# >>> for cat in article.category.all():
# ...     print(cat.title)
# ...
# Android
# Web
# Desktop

# How create an Article?
# user=User.objects.create(username='TestShellUser',password='pt13799820')
# profile=UserProfile.objects.create(user=user)
# category=Category.objects.create(title='testCategory')
# article=Article.objects.create(title='testShellArticle',author=profile,content='content of shell test article')
# article.category.add(category)


def validate_file_extension(path):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(str(path))
    valid_extensions = ['.jpg', '.png', '.jpeg']
    if ext[1].lower() not in valid_extensions:
        raise ValidationError('Unsupported file extension.')


class UserProfile(models.Model):
    avatar = models.FileField(default='default_avatar.png', upload_to='user_avatar/',
                              validators=[validate_file_extension],
                              verbose_name=' آواتار ')
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=' کاربر ')
    description = RichTextField(max_length=512, null=True, blank=True, verbose_name=' شرح حال ')
    color_code = models.CharField(max_length=6, verbose_name=' کد رنگ ', default='ff0000')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.avatar.path)
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.avatar.path)

    class Meta:
        verbose_name = ' بلاگر '
        verbose_name_plural = ' بلاگرها '
        permissions = (
            ("can_read_private_section", "VIP User"),
            ("user_watcher", "User Watcher"),
        )
        # db_table = 'Profile'
        # constraints = [models.CheckConstraint(check=models.Q(user__username__icontains='admin'),
        #                                       name='user__username__icontains_admin')]
        # constraints = [models.CheckConstraint(check=models.Q(color_code='000000'), name='color_code')]

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


# Custom_Manager
class CustomManager(models.Manager):
    def get_queryset(self):
        return super(CustomManager, self).get_queryset().filter(status='published')


class Article(models.Model):
    STATUS_CHOICES = (
        ('draft', 'آماده انتشار'),
        ('published', 'منتشر شده'),
        ('withdrawn', 'برداشته شده'),
    )
    SELECT = (
        ('fa', 'Persian'),
        ('en', 'English'),
        ('fr', 'French'),
    )
    title = models.CharField(max_length=100, verbose_name=' عنوان ')
    slug = models.SlugField(max_length=100, verbose_name=' لینک ',
                            help_text=' برای بهینه سازی در موتورهای جستجو ')
    category = models.ManyToManyField('Category', verbose_name=' دسته بندی ')
    cover = models.FileField(default='default_article_cover.jpeg', upload_to='article_cover/',
                             validators=[validate_file_extension],
                             verbose_name=' جلد ')
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=' نویسنده ')
    language = models.CharField(max_length=2, choices=SELECT, default='en', verbose_name=' زبان ')
    content = RichTextUploadingField(verbose_name=' محتوا ')
    publishedTime = models.DateTimeField(default=datetime.now, null=True, blank=True, verbose_name=' تاریخ انتشار ')
    createdTime = models.DateTimeField(auto_now_add=True, verbose_name=' تاریخ ثبت ')
    updatedTime = models.DateTimeField(auto_now=True, verbose_name=' تاریخ ویرایش ')
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft', verbose_name=' وضعیت ')
    promote = models.BooleanField(default=False)

    def display_category(self):
        return ', '.join([category.title for category in self.category.all()])

    display_category.short_description = 'دسته بندی'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.cover.path)
        if img.height < 600 or img.width < 800:
            output_size = (800, 600)
            img.thumbnail(output_size)
            img.save(self.cover.path)

    objects = models.Manager()  # The default manager.
    published_objects = CustomManager()  # Custom manager.

    class Meta:
        verbose_name = ' مقاله '
        verbose_name_plural = ' مقاله ها '

    def __str__(self):
        return f'{self.title}'


class Category(models.Model):
    cover = models.FileField(default='default_category_cover.jpg', upload_to='category_cover/',
                             validators=[validate_file_extension], verbose_name=' جلد ')
    created_at = models.DateTimeField(default=datetime.now, verbose_name=' تاریخ ایجاد ')
    title = models.CharField(max_length=100, verbose_name=' عنوان ')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.cover.path)
        if img.height < 600 or img.width < 800:
            output_size = (800, 600)
            img.thumbnail(output_size)
            img.save(self.cover.path)

    class Meta:
        verbose_name = ' دسته بندی '
        verbose_name_plural = ' دسته بندی ها '

    def __str__(self):
        return '{}'.format(self.title)


class ContactUs(models.Model):
    name = models.CharField(max_length=20, verbose_name='نام فرستنده')
    email = models.EmailField(verbose_name='ایمیل فرستنده')
    text = RichTextField(max_length=500, verbose_name='متن فرستاده شده')
    date = models.DateTimeField(auto_now_add=True, verbose_name='تاریخ ارسال')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ' تماس '
        verbose_name_plural = ' تماس ها '

    def get_absolute_url(self):
        return reverse('blog:contact')


class HandleBaseEmail(models.Model):
    email = models.EmailField(verbose_name='ایمیل عضویت')

    class Meta:
        verbose_name = ' ایمیل عضویت '
        verbose_name_plural = ' ایمیل های عضویت '
