from ckeditor.widgets import CKEditorWidget
from django import forms
from .models import ContactUs, HandleBaseEmail, Article


class ContactUsForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'name': 'cName',
            'id': 'cName',
            'class': 'full-width',
            'placeholder': 'نام',
            'type': 'text',
            'style': 'direction: rtl',
        })
        self.fields['email'].widget.attrs.update({
            'name': 'cEmail',
            'id': 'cEmail',
            'class': 'full-width',
            'placeholder': 'ایمیل',
            'type': 'text',
            'style': 'direction: rtl',
        })
        self.fields['text'].widget.attrs.update({
            'name': 'cMessage',
            'id': 'cMessage',
            'class': 'full-width',
            'placeholder': 'پیام',
            'type': 'textarea',
            'style': 'direction: rtl',
        })

    class Meta:
        model = ContactUs
        fields = '__all__'


class CreateArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'content', 'category', 'cover', 'author', 'language', 'status', 'promote', 'publishedTime']


class SubscribeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({
            'class': 'email',
            'placeholder': 'Email Address',
            'type': 'email',
        })

    class Meta:
        model = HandleBaseEmail
        fields = '__all__'
