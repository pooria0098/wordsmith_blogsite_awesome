from django.urls import path
from . import views

app_name = 'blog'
urlpatterns = [
    path('', view=views.ArticlesListView.as_view(), name='index'),
    path('article/<int:pk>/', view=views.ArticleDetailView.as_view(), name='article_detail'),
    path('contact/', view=views.ContactPage.as_view(), name='contact'),
    path('category/', view=views.CategoryArticlesListView.as_view(), name='category'),
    path('category/web/', view=views.CategoryWebArticlesListView.as_view(), name='category_web'),
    path('category/android/', view=views.CategoryAndroidArticlesListView.as_view(), name='category_android'),
    path('category/desktop/', view=views.CategoryDesktopArticlesListView.as_view(), name='category_desktop'),
    path('about/', view=views.AboutPage.as_view(), name='about'),
    path('singlePage/', view=views.SingleStandardPage.as_view(), name='singlePage'),
    path('videoPage/', view=views.VideoPage.as_view(), name='videoPage'),
    path('audioPage/', view=views.AudioPage.as_view(), name='audioPage'),
    # CRUD urls
    path('article/create/', views.CreateArticle.as_view(), name='article_create'),
    path('article/<int:pk>/update/', views.UpdateArticle.as_view(), name='article_update'),
    path('article/<int:pk>/delete/', views.DeleteArticle.as_view(), name='article_delete'),
]
